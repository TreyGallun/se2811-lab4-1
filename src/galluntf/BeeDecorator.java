package galluntf;

public class BeeDecorator extends Bee{
    Bee bee;
    public void changeEnergy(int delta){}
    public void step(){
        bee.step();
    }
    BeeDecorator(Bee bee){
        super(bee.garden, bee.location, bee.energy);
        this.bee = bee;
    }
}
