package galluntf;

import java.awt.*;

/**
 * A stationary energy source (or drain) for the Bees.
 */
public abstract class Flower extends GardenObject {
    /**
     * Constructs a new Flower within the specified garden.
     * @param garden Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public Flower(Garden garden, Point location) {
        super(garden, location);
    }

    /** Responds to Bee visiting this flower */
    public abstract void visitedBy(Bee b);
    public abstract int type();
}
