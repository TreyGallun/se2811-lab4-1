package galluntf;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.awt.*;

/**
 * A Bee that seeks nectar from flowers to continue living
 */
public abstract class Bee extends GardenObject {
    /**
     * The amount of nectar (life) that the Bee has remaining
     */
    protected int energy;

    /**
     * Text displayed next to the Bee's icon within the Garden.
     */
    protected Text energyDisplay = new Text("0");

    /**
     * Constructs a new Bee within the specified garden.
     * @param garden Garden in which the Bee lives
     * @param location The Bee's initial location. Not currently checked if out of bounds.
     * @param initialEnergy The Bee's initial energy. Will be set to 0 if greater than 0.
     */
    public Bee(Garden garden, Point location, int initialEnergy) {
        super(garden, location);
        setEnergy(initialEnergy);
    }

    /**
     * Sets the Bee's exact energy level.
     * Bee's energy will not go below zero.
     * @param amount New energy level. If negative, energy is set to 0.
     */
    private void setEnergy(int amount) {
        energy = Math.max(0, amount);
    }

    /**
     * Adds or removes the the Bee's energy.
     * Bee's energy will not go below zero.
     * @param delta the amount to increase the energy by (reduce if negative)
     */
    public void changeEnergy(int delta) {
        setEnergy(energy + delta);
        energyDisplay.setText("" + energy);
    }

    public int getEnergy() {
        return energy;
    }

    /**
     * Move to the specified location.
     * (0,0) is the top left corner of the screen.
     * If the specified location is out of bounds, the Bee will
     * be placed within bounds.
     *
     * If the Bee lands on a flower, it will visit that flower.
     *
     * @param newX horizontal coordinate. Positive to the right.
     * @param newY vertical coordinate. Positive downward.
     */
    private void moveTo(double newX, double newY) {
        changeEnergy(-1);
        location.setLocation(Math.min(garden.MAX_X, Math.max(0, newX)),
                             Math.min(garden.MAX_Y, Math.max(0, newY)));

        Flower closestFlower = garden.closestFlowerTo(location);
        if (onTopOf(closestFlower))
            closestFlower.visitedBy(this);
    }

    /**
     * Moves the bee relative to its current position.
     * @param deltaX horizontal coordinate of step. Positive to the right
     * @param deltaY vertical coordinate of step. Positive downward
     */
    protected void moveBy(double deltaX, double deltaY) {
        moveTo(location.getX() + deltaX, location.getY() + deltaY);
    }

    /**
     * Calculates how the Bee should move.
     * This method should call moveBy as its last action.
     */
    public void step(){
        changeEnergy(-1);
        energyDisplay.setText("" + getEnergy());
    }

    /**
     * Load the icon used to display the Bee within the Garden.
     * @param path Path to icon to load
     */
    @Override
    public void loadImage(String path) {
        Pane beePane = new VBox();
        ImageView beeIV = new ImageView(new Image("file:" + path));
        beeIV.setPreserveRatio(true);
        beeIV.setFitWidth(50.0);
        beePane.getChildren().add(beeIV);
        beePane.getChildren().add(energyDisplay);
        energyDisplay.setFill(Color.RED);
        energyDisplay.setText("" + getEnergy());
        image = beePane;
    }
}
