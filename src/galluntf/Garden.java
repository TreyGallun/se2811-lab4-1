package galluntf;

import java.awt.*;
import java.util.ArrayList;

/**
 * The Garden in which Blowers grow, and Bees buss around looking for flowers.
 */
public class Garden {
    protected ArrayList<Flower> flowers = new ArrayList<>();
    protected ArrayList<Bee> bees = new ArrayList<>();

    /**
     * Maximum distance a Bee may move along horizontal and vertical axes.
     */
    public final static double STEP_DISTANCE = 22.0;

    /**
     * Size of garden in STEP_DISTANCEs
     */
    public final static int    SQUARES_IN_GARDEN = 10;

    /**
     * Size of garden in pixels
     */
    public final static double MAX_X = (SQUARES_IN_GARDEN - 1) * STEP_DISTANCE * 2,
                               MAX_Y = (SQUARES_IN_GARDEN - 1) * STEP_DISTANCE * 2;

    /**
     * Add flowers to the garden
     * @param numFlowers Number of flowers to add.
     */
    public void addRandomFlowers(int numFlowers) {
        for(int i = 0; i < numFlowers; ++i) {
            Point loc = new Point((int)(Math.random() * MAX_X),
                                  (int)(Math.random() * MAX_Y));
            Flower f;
            if (Math.random() > 0.2) {
                f = new Daisy(this, loc);
                f.loadImage("daisy.jpg");
            } else {
                f = new VenusBeeTrap(this, loc);
                f.loadImage("nightshade.jpg");
            }
            flowers.add(f);
        }
    }

    /**
     * Add bees to the garden.
     * @param numBees Number of Bees to add.
     */
    public void addRandomBees(int numBees) {
        for(int i = 0; i < numBees; i++) {
            Point loc = new Point((int)(Math.random() * MAX_X),
                                  (int)(Math.random() * MAX_Y));
            Bee b;
            double rand = Math.random() * 5;
            if (rand < 1) {
                b = new RandomPathBee(new BasicBee(this, loc, 20));
            } else if(rand < 2){
                b = new ScanBee(new BasicBee(this, loc, 200));
            }else if (rand < 3){
                if(Math.random() > .5) {
                    b = new ShieldBee(new ScanBee(new BasicBee(this, loc, 200)));
                }else {
                    b = new ShieldBee(new RandomPathBee(new BasicBee(this, loc, 20)));
                }
            }else if (rand < 4){
                if(Math.random() > .5) {
                    b = new FastBee(new ScanBee(new BasicBee(this, loc, 200)));
                }else {
                    b = new FastBee(new RandomPathBee(new BasicBee(this, loc, 20)));
                }
            }else {
                if(Math.random() > .5) {
                    b = new FlowerBee(new ScanBee(new BasicBee(this, loc, 200)));
                }
                else {
                    b = new FlowerBee(new RandomPathBee(new BasicBee(this, loc, 20)));
                }
            }
            bees.add(b);
        }
    }

    /**
     * Find the Flower closest to a given location.
     *
     * (This method helps Bees find flowers and determine if they should tag a flower.)
     *
     * @param location Point to find flower near.
     * @return The closest flower to location.
     */
    public Flower closestFlowerTo(Point location) {
        double minDistance = Double.MAX_VALUE;
        Flower closestFlower = null;
        for(Flower f : flowers) {
            if (location.distance(f.getLocation()) < minDistance) {
                closestFlower = f;
                minDistance = location.distance(f.getLocation());
            }
        }
        return closestFlower;
    }

    /**
     * Finds a random flower within the Garden.
     * @return The random flower.
     */
    public Flower randomFlower() {
        int flowerIndex = (int)(Math.random() * flowers.size());
        return flowers.get(flowerIndex);
    }

    public ArrayList<Flower> getFlowers() {
        return flowers;
    }

    public ArrayList<Bee> getBees() {
        return bees;
    }
}
