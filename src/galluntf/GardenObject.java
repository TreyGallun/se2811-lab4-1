package galluntf;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.awt.*;

/**
 * Something (e.g. a Bee or Flower) within the Garden.
 */
public abstract class GardenObject {
    protected Point location;
    protected Node image;
    protected Garden garden;

    /**
     * @param garden Garden in which the thing lives
     * @param location The thing's initial location. Not currently checked if out of bounds.
     */
    public GardenObject(Garden garden, Point location) {
        this.garden = garden;
        this.location = location;
    }

    /**
     * @param other Another Garden object
     * @return Euclidean distance to other
     */
    public double distanceTo(GardenObject other) {
        return location.distance(other.location);
    }

    /**
     * Check this is on top of other.
     * @param other Another Garden object
     * @return true if other is within circle of radius Garden.STEP_DISTANCE.
     */
    public boolean onTopOf(GardenObject other) {
        return distanceTo(other) < Garden.STEP_DISTANCE;
    }

    /**
     * Loads an icon for this object.
     * @param path path to icon
     */
    public void loadImage(String path) {
        ImageView objImage = new ImageView(new Image("file:" + path));
        objImage.setPreserveRatio(true);
        objImage.setFitWidth(50.0);
        image = objImage;
    }

    public Point getLocation() {
        return location;
    }

    public Node getImage() {
        return image;
    }

    /**
     * Updates the Bee's location within the containing GUI.
     */
    public void showOnPane() {
        image.setLayoutX(location.getX());
        image.setLayoutY(location.getY());
    }
}
