package galluntf;

import java.awt.*;

/**
 * A dangerous flower that takes energy or kills bees that visit it.
 */
public class VenusBeeTrap extends Flower {
    public static final int VISIT_COST = 3;
    public boolean trippedTrap = false;

    /**
     * @param garden Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public VenusBeeTrap(Garden garden, Point location) {
        super(garden, location);
    }


    /**
     * "Greets" the Bee by zapping some energy from it.
     * Once the flower has been visited once it is "tripped", and there is a
     * good chance it will simply kill the Bee.
     *
     * @param b The Bee to "greet"
     */
    @Override
    public void visitedBy(Bee b) {
        if (trippedTrap || Math.random() < 0.75)
            b.changeEnergy(-VISIT_COST);
        else {
            b.changeEnergy(-1 * b.getEnergy());
            trippedTrap = true;
        }
    }
    @Override
    public int type(){
        return 1;
    }
}
