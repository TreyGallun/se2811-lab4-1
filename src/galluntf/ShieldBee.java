package galluntf;

import javafx.scene.Node;

public class ShieldBee extends BeeDecorator{
    Boolean shield = true;
    ShieldBee(Bee bee){
        super(bee);
        bee.loadImage("bee-3.jpg");
    }

    @Override
    public void step(){
        bee.step();
        bee.energyDisplay.setText("" + bee.getEnergy());
    }

    @Override
    public void changeEnergy(int delta) {
        if(shield)
            shield = false;
        else
            bee.changeEnergy(delta);
    }
    @Override
    public Node getImage() {
        return bee.image;
    }
    /**
     * Updates the Bee's location within the containing GUI.
     */
    @Override
    public void showOnPane() {
        bee.image.setLayoutX(bee.location.getX());
        bee.image.setLayoutY(bee.location.getY());
    }
}
