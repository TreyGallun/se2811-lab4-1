package galluntf;

import java.awt.*;

/**
 * A friendly flower providing nectar.
 */
public class Daisy extends Flower {
    /**
     * The amount of nectar to give the first Bee that visits this flower.
     */
    private static final int DELTA_NECTAR = 10;

    protected boolean hasNectar = true;

    /**
     * @param garden Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public Daisy(Garden garden, Point location) {
        super(garden, location);
    }

    /**
     * Gives a Bee nectar, but only on its first visit
     * @param b The Bee to greet
     */
    @Override
    public void visitedBy(Bee b) {
        if (hasNectar)
            b.changeEnergy(DELTA_NECTAR);
        hasNectar = false;
    }

    @Override
    public int type(){
        return 0;
    }
}
