package galluntf;

import javafx.scene.Node;

public class FastBee extends BeeDecorator{
    FastBee(Bee bee){
        super(bee);
        bee.loadImage("bee-4.png");
    }

    @Override
    public void step() {
        double x = getLocation().getX();
        double y = getLocation().getY();
        bee.step();
        bee.location.setLocation(bee.location.getX() + (bee.location.getX() - x) * 2, bee.location.getY() + (bee.location.getY() - y) * 2);
        bee.energyDisplay.setText("" + bee.getEnergy());
    }
    @Override
    public Node getImage() {
        return bee.image;
    }
    /**
     * Updates the Bee's location within the containing GUI.
     */
    @Override
    public void showOnPane() {
        bee.image.setLayoutX(bee.location.getX());
        bee.image.setLayoutY(bee.location.getY());
    }
}
