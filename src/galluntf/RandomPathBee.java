package galluntf;

import javafx.scene.Node;

import java.awt.*;

/**
 * A Bee which picks random flower, flies to it, then picks another
 */
public class RandomPathBee extends BeeDecorator {
    protected Flower target = null;

    /**
     */
    public RandomPathBee(Bee bee) {
        super(bee);
        bee.loadImage("bee-1.jpg");
    }

    /**
     * Picks a random flower and flies on the diagonal closest to it.
     * Moves by Garden.STEP_DISTANCE along each axis. (So total distance along diagonal is sqrt(2)*Garden.STEP_DISTANCE.)
     * Upon reaching the flower, it visits it picks a new flower.
     */
    @Override
    public void step() {
        bee.step();
        if(bee.getEnergy() > 0) {
            if (target == null) {
                target = garden.randomFlower();
            }
            if (bee.onTopOf(target)) {
                target.visitedBy(this);
                target = null;
            } else {
                double deltaX = Math.copySign(Garden.STEP_DISTANCE,
                        target.getLocation().getX() - bee.location.getX());
                double deltaY = Math.copySign(Garden.STEP_DISTANCE,
                        target.getLocation().getY() - bee.location.getY());
                bee.moveBy(deltaX, deltaY);
            }
        }
    }
    @Override
    public Node getImage() {
        return bee.image;
    }
    /**
     * Updates the Bee's location within the containing GUI.
     */
    @Override
    public void showOnPane() {
        bee.image.setLayoutX(bee.location.getX());
        bee.image.setLayoutY(bee.location.getY());
    }
}
