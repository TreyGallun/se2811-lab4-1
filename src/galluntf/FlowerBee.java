package galluntf;

import javafx.scene.Node;

public class FlowerBee extends BeeDecorator{
    protected Flower target = null;
    FlowerBee(Bee bee){
        super(bee);
        bee.loadImage("bee-5.png");
    }
    @Override
    public Node getImage() {
        return bee.image;
    }
    /**
     * Updates the Bee's location within the containing GUI.
     */
    @Override
    public void showOnPane() {
        bee.image.setLayoutX(bee.location.getX());
        bee.image.setLayoutY(bee.location.getY());
    }
    @Override
    public void step(){
        bee.step();
        if(bee.getEnergy() > 0) {
            while (target == null || target.type() != 1) {
                target = garden.randomFlower();
            }
            if (bee.onTopOf(target)) {
                target.visitedBy(this);
                target = null;
            } else {
                double deltaX = Math.copySign(Garden.STEP_DISTANCE,
                        target.getLocation().getX() - bee.location.getX());
                double deltaY = Math.copySign(Garden.STEP_DISTANCE,
                        target.getLocation().getY() - bee.location.getY());
                bee.moveBy(deltaX, deltaY);
            }
        }
        bee.energyDisplay.setText("" + bee.getEnergy());
    }
}
