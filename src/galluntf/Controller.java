package galluntf;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * Controller for the garden window.
 * Layout described in garden.fxml
 */
public class Controller {

    @FXML
    private Pane gardenPane, legendPane;

    private Garden garden = new Garden();

    /**
     * Sets up garden and adds Bees and Flowers.
     */
    @FXML
    public void initialize() {
        gardenPane.setStyle("-fx-background-color: linear-gradient(to bottom right, derive(goldenrod, 20%), derive(goldenrod, -40%));");
        gardenPane.setFocusTraversable(true); // ensure garden pane will receive keypresses
        garden.addRandomBees(4);
        garden.addRandomFlowers(10);
        for(Flower f: garden.getFlowers() ) {
            gardenPane.getChildren().add(f.getImage());
            f.showOnPane();
        }
        for(Bee b: garden.getBees()) {
            gardenPane.getChildren().add(b.getImage());
            b.showOnPane();
        }
        setupLegendPane();
    }

    /**
     * Helper method for setupLegendPane
     * @param path path of image to load
     * @return the the loaded image
     */
    private ImageView loadPicture(String path) {
        ImageView i = new ImageView(new Image("file:" + path));
        i.setFitWidth(50.0);
        i.setFitHeight(50.0);
        return i;
    }

    /**
     * Creates simple graphical legend
     */
    private void setupLegendPane() {
        legendPane.getChildren().add(loadPicture("daisy.jpg"));
        legendPane.getChildren().add(new Text("Daisy"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("nightshade.jpg"));
        legendPane.getChildren().add(new Text("Venus Bee Trap"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-1.jpg"));
        legendPane.getChildren().add(new Text("Random Path Bee"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-2.jpg"));
        legendPane.getChildren().add(new Text("Scan Bee"));

        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-3.jpg"));
        legendPane.getChildren().add(new Text("Shield Bee"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-4.png"));
        legendPane.getChildren().add(new Text("Fast Bee"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-5.png"));
        legendPane.getChildren().add(new Text("Flower Bee"));
    }

    /**
     * Simluates one step forward for all Bees in the game
     * @param event the key pressed. Must be the right arrow for a step to occur.
     */
    @FXML
    public void onKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.RIGHT) {
            for (Bee b : garden.getBees()) {
                b.step();
                if (b.getEnergy() > 0)
                    b.showOnPane();
            }
        }
    }
}
