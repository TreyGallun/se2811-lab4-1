package galluntf;

import javafx.scene.Node;

import java.awt.*;

/** ScanBee moves top to bottom and then dies once it reaches the bottom right */
public class ScanBee extends BeeDecorator {
    private boolean goingRight = true;

    /**
     */
    public ScanBee(Bee bee) {
        super(bee);
        bee.loadImage("bee-2.jpg");
    }

    @Override
    public Node getImage() {
        return bee.image;
    }

    /**
     * Flies in a lawnmower pattern over the garden.
     *
     * Scans back and forth in horizontal lines.
     *
     * Dies upon reaching the bottom corner!
     */
    @Override
    public void step() {
        bee.step();
        if(bee.getEnergy() > 0) {
            long x = Math.round(bee.location.getX());
            long y = Math.round(bee.location.getY());
            if (goingRight) {
                if (x + garden.STEP_DISTANCE < garden.MAX_X)
                    bee.moveBy(garden.STEP_DISTANCE, 0);
                else {
                    goingRight = false;
                    bee.moveBy(0, garden.STEP_DISTANCE);
                }
            } else {
                if (x > 0)
                    bee.moveBy(-garden.STEP_DISTANCE, 0);
                else {
                    goingRight = true;
                    bee.moveBy(0, garden.STEP_DISTANCE);
                }
            }
            // if reach bottom right corner, the bee dies
            if (bee.location.getX() + Garden.STEP_DISTANCE >= bee.garden.MAX_X
                    && bee.location.getY() + Garden.STEP_DISTANCE >= garden.MAX_Y) {
                // the bee's work is done and it dies
                bee.energy = 0;
            }
        }
    }
    /**
     * Updates the Bee's location within the containing GUI.
     */
    @Override
    public void showOnPane() {
        bee.image.setLayoutX(bee.location.getX());
        bee.image.setLayoutY(bee.location.getY());
    }
}
