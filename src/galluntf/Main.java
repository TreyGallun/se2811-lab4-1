package galluntf;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    /**
     * Loads the garden window.
     * @param primaryStage Main window to populate.
     * @throws Exception if there is a problem loading the garden.fxml
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("garden.fxml"));
        primaryStage.setTitle("Garden Simulator");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    /** Launches GUI */
    public static void main(String[] args) {
        launch(args);
    }
}
